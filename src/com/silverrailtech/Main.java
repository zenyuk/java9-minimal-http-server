package com.silverrailtech;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Main {

    // to use:  curl http://localhost:4250/java

    public static void main(String[] args) throws IOException {
        String payload = "minimal java http server \n";
        HttpServer server = HttpServer.create(new InetSocketAddress(4250), 0);
        HttpContext context = server.createContext("/java");
        context.setHandler((he) -> {
            he.sendResponseHeaders(200, payload.getBytes().length);
            final OutputStream output = he.getResponseBody();
            output.write(payload.getBytes());
            output.flush();
            he.close();
        });
        server.start();
    }
}
